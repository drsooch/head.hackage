{-# LANGUAGE DeriveDataTypeable #-}

module Drvrun022 where

-- GHC 6.4.1 output "testz" in z-encoded form!

import Data.Generics
import System.IO

data TestZ = TestZ { testz :: Int }
             deriving (Show, Read, Eq, Data, Typeable)

main stdout _ = hPrint stdout $ constrFields . toConstr $ TestZ { testz = 2 }
