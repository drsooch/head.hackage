module Concio002 where
import System.Process
import System.IO
import Control.Concurrent

main stdout _ = do
  (hin,hout,herr,ph) <- runInteractiveProcess "cat" [] Nothing Nothing
  forkIO $ do threadDelay 100000
              hPutStrLn stdout "child"
              hFlush stdout
              hPutStrLn hin "msg"
              hFlush hin
  hPutStrLn stdout "parent1"
  hGetLine hout >>= hPutStrLn stdout
  hPutStrLn stdout "parent2"
