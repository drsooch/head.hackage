{-# LANGUAGE OverloadedStrings #-}

module MakeConstraints where

import qualified Distribution.Package as Cabal
import Distribution.Text
import Distribution.Types.Version hiding (showVersion)

import qualified Data.Set as S
import qualified Data.Map.Strict as M

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Text.PrettyPrint.ANSI.Leijen (Doc, vcat, (<+>))

import Utils

bootPkgs :: S.Set Cabal.PackageName
bootPkgs = S.fromList
  [ "base"
  , "template-haskell"
  , "time"
  , "ghc"
  , "ghc-prim"
  , "integer-gmp"
  , "bytestring"
  , "text"
  , "binary"
  , "ghc-bignum"
  -- Cabal is not in this list because of complications with the Cabal/Cabal-syntax
  -- upgrade. See !205 and https://github.com/haskell/cabal/issues/7974
  -- , "Cabal"
  ]

allowNewer :: S.Set Cabal.PackageName -> Doc
allowNewer pkgs =
  "allow-newer:" PP.<$$> PP.indent 2 pkgsDoc
  where
    pkgsDoc = PP.vcat $ PP.punctuate "," $ map prettyPackageName $ S.toList pkgs

installedConstraints :: S.Set Cabal.PackageName -> S.Set Cabal.PackageName -> Doc
installedConstraints bootPkgs patchedPkgs =
  "constraints:" PP.<$$> PP.indent 2 pkgsDoc
  where
    pkgsDoc = PP.vcat $ PP.punctuate ","
      [ prettyPackageName bootPkg <+> "installed"
      | bootPkg <- S.toList bootPkgs
      , bootPkg `S.notMember` patchedPkgs
      ]

versionConstraints :: [(Cabal.PackageName, Version)] -> Doc
versionConstraints pkgs =
  "constraints:" PP.<$$> PP.indent 2 body
  where
    body :: Doc
    body = vcat $ PP.punctuate ","
     [ prettyPackageName pkg <+> versionConstraints vers
     | (pkg, vers) <- M.toList pkgVersions
     ]

    versionConstraints :: S.Set Version -> Doc
    versionConstraints vers =
      PP.hcat $ PP.punctuate " || "
      [ "==" <> prettyVersion ver
      | ver <- S.toAscList vers
      ]

    pkgVersions :: M.Map Cabal.PackageName (S.Set Version)
    pkgVersions = M.fromListWith (<>)
      [ (pkg, S.singleton ver)
      | (pkg, ver) <- pkgs
      ]

makeConstraints :: FilePath -- ^ patch directory
                -> IO Doc
makeConstraints patchDir = do
  patches <- findPatchedPackages patchDir
  let patchedPkgs = S.fromList $ map fst patches
      doc = PP.vcat
        [ allowNewer bootPkgs
        , ""
        , installedConstraints bootPkgs patchedPkgs
        , ""
        , versionConstraints patches
        ]
  return doc
